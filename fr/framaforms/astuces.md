# Astuces

## Je souhaite recevoir un mail à chaque réponse effectuée sur mon formulaire

Pour recevoir un mail à chaque fois que quelqu'un répond à votre formulaire, vous devez&nbsp;:

  * aller dans l'onglet **Formulaire**
  * cliquer sur **Courriels**
  * cliquer sur le bouton **Ajouter un courriel standard** dans la section **Standard emails (always send)**
  * dans la section **Adresse de courriel du destinataire** vous devez entrer votre adresse mail dans **Personnalisé**
  * vérifier que **Activer envoi** est coché
  * laisser ou modifier le modèle de courriel - le modèle par défaut sera :

    ```
    Soumis le [submission:date:long]
    Soumis par l'utilisateur : [submission:user]
    Les valeurs soumises sont:
    [submission:values]

    Les résultats de cette soumission peuvent être vus ici :
    [submission:url]
    ```

    c'est-à-dire :

    ```
    Soumis le Lundi, décembre 17, 2018 - 11:15
    Soumis par l'utilisateur : Anonyme
    Les valeurs soumises sont:

    Nombre de personnes: 3
    Un message à laisser ?: Je serais probablement en retard !

    Les résultats de cette soumission peuvent être vus ici :
    https://framaforms.org/node/XXXXX/submission/XXXXXXXX
    ```

  * cliquer sur **Enregistrer les paramètres de courriel**

## Je souhaite que les personnes qui répondent à mon formulaire reçoivent une copie de leurs réponses par mail

Pour que les personnes reçoivent par mail leurs réponses, vous devez&nbsp;:

  1. ajouter un composant **[Courriel](./composants.html#courriel)** où la personne devra fournir son adresse (pensez à le rendre obligatoire si besoin)
  * cliquer sur **Courriels**
  * cliquer sur le bouton **Ajouter un courriel standard** dans la section **Standard emails (always send)**
  * dans la section **Adresse de courriel du destinataire** vous devez cliquer sur **Composant :** et choisir le titre du composant email que vous avez paramétré
  * vérifier que **Activer envoi** est coché
  * laisser ou modifier le modèle de courriel - le modèle par défaut sera :

    ```
    Soumis le [submission:date:long]
    Soumis par l'utilisateur : [submission:user]
    Les valeurs soumises sont:
    [submission:values]
    ```

    c'est-à-dire :

    ```
    Soumis le Lundi, décembre 17, 2018 - 11:15
    Soumis par l'utilisateur : Anonyme
    Les valeurs soumises sont:

    Nombre de personnes: 3
    Un message à laisser ?: Je serais probablement en retard !
    ```

  * cliquer sur **Enregistrer les paramètres de courriel**

## Je souhaite envoyer des mails en fonction des réponses

Je veux envoyer un mail à une adresse différente en fonction du ou des comités séléctioné :

  * je vais dans l'onglet **Courriels**
  * je sélectionne le composant
  * j'ajoute les adresses mail en fonction du comité
  * je sauvegarde (tout en bas)

![composant selections multiples](images/astuces_forms_courriel_comite.png)
![courriel en fonction du composant](images/astuces_forms_courriel_comite2.png)

## Je souhaite un système de CAPTCHA

* j'ajoute un champ texte à mon formulaire (en cochant **Requis** dans l'onglet **Validation** du composant)
* dans l'onglet **Validation du formulaire** j'ajoute une règle **Motif**
* je remplis les champs comme sur l'image ci-dessous

![Règle Motif Framaforms](images/astuces_forms_captcha.png)

## Je souhaite un champ « autre » libre

Je voudrais qu'en cliquant sur le choix « autre », les répondant⋅e⋅s puissent mettre un choix libre. Pour cela :

* je créé un composant « Boutons radio » ![composant bouton radio](images/astuces_forms_autre1.png)
* je créé un composant « Champ texte » (ici, avec le titre « Autre ?») ![composant texte autre](images/astuces_forms_autre2.png)
* j'enregistre
* je vais dans « Champs conditionnels »
* je créé la règle : « Si la mascotte est autre alors Autre ? est affiché » (si le champ autre du composant « Boutons radio » est « autre » alors on affiche le composant « Champ texte » - sinon, il ne le sera pas) ![conditions](images/astuces_forms_autre3.png)

Résultats :

![Résultat autre caché](images/astuces_forms_autre4.png)
![Résultat autre visible](images/astuces_forms_autre5.png)

## Je souhaite afficher des images pour les choix

Vous pouvez le faire en y insérant une photo **déjà disponible sur le web** et en utilisant la balise HTML `img`. Par exemple, si vous souhaitez insérer l'image du logo Framasoft (disponible en cliquant-droit sur le logo de la barre de navigation haute et en cliquant sur **Afficher l'image**)  dans votre composant **Boutons radios**, vous devez insérer `<img src="https://framasoft.org/nav/img/logo.png" />` dans un champ.

![image dans un composant boutons radios](images/astuces_forms_images_composant_radios.png)

## Je souhaite afficher une image en en-tête de mon Formulaire

### Depuis Framapic (recommandé)

Vous devez commencer par envoyer votre image sur notre service d'hébergement temporaire d'images [Framapic](https://framapic.org/) ([voir le tutoriel vidéo](../../fr/lutim/#tutoriel-vidéo)).
Ensuite (dans l'onglet **Modifier**)&nbsp;:

  * cliquez sur l'icône permettant d'ajouter l'adresse de l'image framapic
  ![image montrant l'icône à utiliser pour insérer une image](images/astuces_forms_insert_image_header.png)
  * dans la fenêtre qui s'ouvre&nbsp;:
    1. collez l'adresse de l'image framapic dans le champ **Image URL**
      ![image montrant le champ image url](images/astuces_forms_insert_url.png)
    * cliquez sur **Insert**
  * cliquez sur le bouton **Enregistrer** en bas de la page

### Directement dans Framaforms

Vous avez aussi la possibilité d'insérer directement une image depuis Framaforms. Les fichiers doivent peser moins de 2 Mo. Les extensions autorisées sont : `png`, `gif`, `jpg` et `jpeg`. Pour ce faire, vous devez (dans l'onglet **Modifier**)&nbsp;:

![image montrant les étapes pour ajouter une image](images/astuces_forms_insert_image_header_direct.png)

  1. cliquer sur **Image**
  * cliquer sur **Parcourir…** pour récupérer l'image sur votre ordinateur
  * cliquer sur **Transférer**

Il vous suffit alors de cliquer sur le bouton **Enregistrer** en bas de la page



## Je souhaite visualiser mes résultats dans un Framacalc

Dans Framaforms, vous devez :

  * vous rendre dans l'onglet **Résultats**, puis **Téléchargement**
  * sélectionner `Virgule` dans **Format du texte délimité** pour que ce soit au format `.csv` (`c` étant pour `comma` qui signifie virgule en anglais)
  * cliquer sur **Téléchargement** pour télécharger le fichier sur votre ordinateur

Dans Framacalc :

  * allez dans l'onglet **Presse-papier**
  * cochez **Format CSV**
  * collez le contenu de votre fichier `csv` framaforms dans le cadre
  ![forms dans calc](images/astuces_forms_calc.png)
  * cliquez sur le bouton **Charger le presse-papier de SocialCalc avec ceci**
  * dans l'onglet **Édition**, cliquez sur l'icône **Coller** (ou en faisant `CTRL+v`)

## Je souhaite créer une échelle sémantique différentielle

Pour créer une [échelle sémantique différentielle](https://fr.wikipedia.org/wiki/%C3%89chelle_s%C3%A9mantique_diff%C3%A9rentielle), il est possible d'utiliser le composant **Grille**. Pour ce faire, vous devez&nbsp;:
  * mettre votre question en titre
  * dans l'onglet **Options**, mettre des `l` (la lettre `L` en minuscule) et `|` (`Alt Gr`+`6`) au milieu
  * supprimer des questions dans la zone **Questions**
  * mettre dans la question restante vos valeurs (ici, "froid" et "chaud") : `Froid | Chaud` ; en mettant `|` cela permet de mettre une valeur de chaque côté de la ligne

![forms paramétrage échelle](/images/astuces-forms-echelle.png)

Résultat :

![forms résultat échelle](images/astuces-forms-echelle-resultat.png)

Pour éviter le [bug dans les résultats avec le composant Grille](https://framagit.org/framasoft/framaforms/issues/12), n'oubliez pas de personnaliser les clés. Vous pouvez, par exemple, cliquer sur **Options** > **Saisie manuelle** et ajouter :

```
-3|l
-2|l
-1|l
n||
1|l
2|l
3|l
```
Pour faire l'exemple ci-dessus.

## Je souhaite rendre un champ obligatoire

Si vous souhaitez que les personnes mettent leur nom (pour un formulaire d'inscription, par exemple), vous pouvez rendre le champ **Nom** obligatoire en&nbsp;:

  * ajoutant un composant **Champ texte**
  * lui donnant le **Titre** «&nbsp;Nom&nbsp;»
  * en cliquant sur l'onglet **Validation** (**1**)
  * en cochant **Requis** (**2**)
  * en cliquant sur **Enregistrer** (**3**)

![Image illustrant les étapes](images/astuces_champ_requis.png)

Les personnes qui répondent aux formulaires ne pourront pas le valider sans avoir entré leur nom dans le champ requis.

<div class="alert alert-info">
  Les champs obligatoires sont marqués d’un <b> * </b> rouge.
</div>

## Comment protéger mon formulaire du spam ?

Pour lutter contre le spam, et tout autre robot, sur votre formulaire Framaforms, vous pouvez ajouter une condition à la validation du formulaire. Ajoutez sur la page **Formulaire** > **Conception du formulaire** un élément **champ texte** à votre formulaire. Son nom peut être "Anti Spam". Entrez en tant que description le texte d’une question n’ayant qu’une réponse évidente pour un humain.

Par exemple :

  * Pour montrer que vous n’êtes pas un robot, entrez la réponse à la question suivante : combien font 3 plus sept ? (réponse écrite en chiffres)
  * Pour montrer que vous n’êtes pas un robot, entrez le mot suivant «&nbsp;antispam&nbsp;» ?
  * Pour montrer que vous n’êtes pas un robot, entrez le mot qui manque dans l’expression «&nbsp;prendre ses jambes à son `***`&nbsp;»
  * Soyez créatifs pour en faire d’autres :)

Sur la page **Formulaire** > **Validation du formulaire**, cliquez sur **Valeur(s) spécifique(s)**. Dans la partie **Composants**, cochez seulement votre nouveau champ texte "Anti Spam". Entrez dans **(Clé) valeur** la solution à la question précédente. Vous pouvez personnaliser le message d’erreur en cas de réponse incorrecte.

## Comment mettre en place une validation des réponses par email ?

Vous voulez vérifier que les adresses email entrées par vos sondés sont correctes ? Pour cela, vous pouvez configurer votre formulaire pour qu’après le remplissage du formulaire, la réponse entrée soit validée par le clic sur un lien envoyé à l’adresse email.

  * Créer un formulaire avec un champ email. Vous pouvez rendre ce champ obligatoire en cliquant sur son onglet "Validation" > "Requis"
  * Toujours en mode modification de formulaire, dans l’onglet "Courriels", créer un nouvel email de confirmation en cliquant sur "Add confirmation request email". Dans "Composant", sélectionner le nom du champ de mail.
  * Toujours dans la configuration de cet email de confirmation, il faut ajouter un gabarit personnalisé qui sera envoyé à l’utilisateur.
  * > Bonjour [submission:user] !
    > Le [submission:date:long], vous avez répondu au formulaire [current-page:url]
    > *IMPORTANT* : pour confirmer votre participation, veuillez simplement cliquer sur le lien suivant : [submission:confirm_url]
    > Votre participation ne sera pas prise en compte tant que vous n’aurez pas effectué cette action.
    > Les valeurs soumises sont:
    > [submission:values]
    > Cordialement.

   * Enregistrez le tout.

L’utilisateur recevra alors un email à l’adresse sélectionnée, et devra cliquer sur l’url de confirmation (sinon, sa participation apparaîtra comme "non confirmée" dans les résultats).

Pour aller plus loin, vous pouvez aussi&nbsp;:

  * renvoyer un email de remerciement après la confirmation
  * le renvoyer sur une page dédiée
  * faire expirer l’URL de confirmation après x jours et y heures
  * etc

## Comment limiter à une réponse par adresse email ?

Les paramètres avancés de votre formulaire vous permettent d’activer la limitation à une réponse qui se base sur l’adresse IP et sur un cookie. Cependant dans différents cas d’usage il peut être préférable d’avoir une limitation d’une réponse par adresse email (ou sur un autre champ du formulaire).

Par exemple, si vous souhaitez que deux personnes puissent répondre facilement depuis le même ordinateur, ou si vous voulez que cette limite ne puisse pas être contournée trop facilement (par exemple en utilisant la navigation privée du navigateur, un VPN ou autres).

Lors de la création du champ que vous voulez unique, dans son onglet **Validation**, sélectionnez la case **Unique**, comme ci-dessous&nbsp;:

![image montrant la case "Unique" cochée](images/forms_champ_unique.png)

## Comment ajouter un titre dans le corps de mon formulaire ?

Pour insérer un titre dans le corps du formulaire, vous pouvez, sur la page **Formulaire** > **Conception du formulaire**&nbsp;:

  * utiliser l’élément **balisage** qui se situe dans la partie droite de la page
  * le déplacer où vous souhaitez dans votre sondage
  * éditer le contenu de ce nouvel élément (dans sa partie **Balisage**)
  * utiliser le HTML pour mettre en page

  Vous pouvez par exemple entrer `<h3>Mon titre</h3>` en replaçant "Mon titre" par le titre que vous souhaitez. Si vous souhaitez un titre plus gros remplacez "h3" par "h2". Vous pouvez, en créant une nouvelle ligne, ajoutez du texte sous le titre.


## Comment limiter à une soumission par utilisateur ?

Vous avez la possibilité de limiter la soumission par utilisateur en&nbsp;:

  * allant dans l’onglet **Formulaire**
  * cliquant sur **Paramètres avancés du formulaire**
  * allant dans la section **Limitation des soumissions par utilisateur**
  * cochant **Limiter chaque utilisateur à** `nombre` **soumission(s) pour**
    * **en tout**
    * **toutes les minutes**
    * **toutes les 5 minutes**
    * **par heure**
    * **tous les jours**
    * **toutes les semaines**
  * cliquant sur le bouton **Enregistrer la configuration**

<div class="alert-info alert">Limiter le nombre de soumissions par utilisateur. Un utilisateur est identifié par son nom de connexion s'il est connecté , ou par son adresse IP et un cookie si anonyme.</div>

## Comment pré-remplir certains champs avec des valeurs fournies dans l'url du formulaire ?

  * Éditer la valeur par défaut du champ en question&nbsp;: `[current-page:query:key]`
    ![édition du champ par défaut d'un composant texte](images/astuces_forms_key_url.png)
  Dans l’exemple ci-dessus, `key` est `nom` (`[current-page:query:nom]`) :
  * Ajouter à l'url de votre formulaire la clé du champ et la valeur à fournir ; Par exemple&nbsp;: `?nom=spf` (`https://framaforms.org/mon-form?nom=spf`) pour faire apparaître `spf` dans le composant texte

Plus d'information en anglais sur [la documentation drupal](https://www.drupal.org/node/296453#webform-url-default).

## Afficher / ne pas afficher la barre de progression

Pour (ne pas) afficher la barre de progression vous devez&nbsp;:

  1. aller dans l'onglet **Formulaire**
  * cliquer sur **Paramètres avancés du formulaire**
  * cliquer sur la section **Barre de progression**
  * (dé)cocher **Afficher la barre de progression**
  * cliquer sur **Enregistrer la configuration**
