# Framaforms

[Framaforms](https://framaforms.org) est un service en ligne libre qui permet de créer, éditer et publier ses formulaires en ligne, puis d’en récolter, analyser et exporter les réponses.

Propulsé par le logiciel libre [Drupal](https://drupal.org) associé au module [Webform.com](https://webform.com), vous pouvez découvrir les fonctionnalités et utilisations possibles de Framaforms en consultant notre [exemple d’utilisation](exemple-d-utilisation.html).


Voici un exemple de formulaire créé sur Framaforms&nbsp;:

![Formulaire final tel que vu par les utilisateurs](images/framaforms_18_visu2.png)



### Pour aller plus loin&nbsp;:

  * [Déframasoftiser Internet](deframasoftiser.html)
  * [Essayer Framaforms](https://framaforms.org)
  * Découvrir [la liste des modèles](https://framaforms.org/templates)(à vous d’en créer !)
  * Le [site de Drupal](https://drupal.org), moteur de Framaforms
  * [Webform.com](https://webform.com)&nbsp;: une alternative très proche de Framaforms, partiellement libre - par l’auteur du module Webform (gratuit avec quelques limitations, mais avec une offre payante si vous ne voulez pas de contraintes)
  * [Installer Framaforms](http://framacloud.org/cultiver-son-jardin/installation-de-framaforms/) sur votre site
  * [Les astuces](astuces.md)
  * [Les fonctionnalités](fonctionnalites.md)
  * [Les composants](composants.md)
  * [Dégooglisons Internet](https://degooglisons-internet.org)
  * [Soutenir Framasoft](https://soutenir.framasoft.org)
