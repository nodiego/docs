# Framaclic

[Framaclic](https://framaclic.org) est un raccourcisseur d'URL qui compte les clics.

![ajout dolo](images/ajoutDoloVFVVOK.png)

Vous pouvez découvrir les fonctionnalités et utilisations de Framaclic en consultant notre [exemple d'utilisation](exemple-d-utilisation.md).

## Pour aller plus loin :

* Un [exemple d'utilisation](exemple-d-utilisation.md)
* Testez [Framaclic](https://framaclic.org/)
    * (optionnel) le [plugin Dolomon pour WordPress](https://fr.wordpress.org/plugins/dolomon/)
* Installez [Dolomon sur votre propre serveur](https://framacloud.org/fr/cultiver-son-jardin/dolomon.html)
* Contribuez [au code de Dolomon](https://framagit.org/luc/dolomon)
* [Découvrez et soutenez le travail de Luc sur son blog](https://fiat-tux.fr/)
