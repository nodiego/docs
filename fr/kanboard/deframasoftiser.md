# Déframasoftiser Framaboard

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter les données de votre espace Framaboard vous devez&nbsp;:

  1. vous rendre dans vos préférences (en cliquant sur votre avatar en haut à droite
  * cliquer sur **Préférences**
  * cliquer sur **Télécharger la base de données (Fichier Sqlite compressé en Gzip)** dans la section **Base de données**
  * enregistrer le fichier sur votre ordinateur

![Image export données](screenshots/kanboard-export-donnees.png)

## Importer

Pour importer une base de données précédemment télécharger (voir [ci-dessus](#exporter)), vous devez&nbsp;:

  1. vous rendre dans vos préférences (en cliquant sur votre avatar en haut à droite
  * cliquer sur **Préférences**
  * cliquer sur **Téléverser la base de données** dans la section **Base de données**
  * cliquer sur **Browse…** pour récupérer le fichier exporté
  * cliquer sur **Téléverser**

## Supprimer son compte Framaboard

Le logiciel que nous utilisons pour Framaboard ne permet pas de supprimer soi-même son espace : vous devez [nous en faire la demande](https://contact.framasoft.org/#framaboard) **depuis l’adresse mail associée au compte administrateur** en spécifiant **l’adresse de votre framaboard**.

Pour supprimer des utilisateurs/utilisatrices : [voir notre documentation](https://docs.framasoft.org/fr/kanboard/user-management.html#supprimer-des-utilisateurs).
