# Framanotes

[Framanotes](https://framanotes.org) est un service en ligne libre qui permet de créer, synchroniser et retrouver ses notes, de manière chiffrée.
Le chiffrement de bout en bout (dont la clé est votre mot de passe) implique que, ne connaissant pas votre mot de passe, **nous ne pouvons pas savoir ce que vous avez noté dans ce service**.

Propulsé par le logiciel libre [Turtl](https://turtlapp.com/), Framanotes vous permet de&nbsp;:

* Créer & modifier des notes textes au format Markdown&nbsp;;
* Donc créer aisément des listes à puces, avec titres, gras et italique&nbsp;;
* Créer & modifier des notes images (jusqu’à 2 Mo par fichier)&nbsp;;
* Créer & modifier des notes fichiers (jusqu’à 2 Mo par fichier)&nbsp;;
* Créer & modifier des notes marque-pages (adresses web)&nbsp;;
* Noter vos mots de passe (allez-y, c’est chiffré&nbsp;!)&nbsp;;
* Trier vos notes par un système d’étiquettes (tags)&nbsp;;
* Rechercher dans vos notes (indexation)&nbsp;;
* Rassembler certaines notes dans des tableaux&nbsp;;
* Partager un ou des tableaux avec vos ami·e·s (qui sont sur Framanotes).

Voici une capture d’un compte Framanotes.

![Exemple d’un compte Framanotes](images/framanote-notes-resultat.png)

Tutoriel pour bien débuter&nbsp;: [Framanotes, exemple d’utilisation](exemple-d-utilisation.html)

### Pour aller plus loin&nbsp;:

-   [Prise en mains](prise-en-mains.html)
-   [Déframasoftiser Internet](deframasoftiser.html)
-   Tester [Framanotes](https://framanotes.org)
    -   Télécharger [les applications Turtl](https://turtlapp.com/download/)
    -   Télécharger les extensions Turtl pour votre navigateur
        ([Firefox](https://addons.mozilla.org/fr/firefox/addon/turtl/) /
        [Chromium-Chrome](https://chrome.google.com/webstore/detail/turtl/dgcojenhfdjhieoglmiaheihjadlpcml))
    -   La [version Web](https://mes.framanotes.org) (expérimentale&nbsp;!)
    -   L’adresse à rentrer dans les applications&nbsp;: <https://api.framanotes.org>
-   [Turtl](https://turtlapp.com/), site officiel (pensez à [les
    soutenir](https://turtlapp.com/donate/)&nbsp;!)
-   Installer [Turtl sur vos
    serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-turtl/)
-   Nos forks
    -   [client](https://framagit.org/framasoft/framanotes-js) (juste un
        peu de texte, des liens en plus)
    -   [serveur](https://framagit.org/framasoft/framanotes-api)
-   [Dégooglisons Internet](https://degooglisons-internet.org)
-   [Soutenir Framasoft](https://soutenir.framasoft.org)
