# Déframasoftiser Framasite wiki

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Export du wiki

Pour exporter votre wiki vous devez&nbsp;:

  * cliquer sur **Administrer**
  * cliquer sur **DokuWiki Advanced: Export Utility** dans la section **Extensions**
  * cocher sur **Include sub-namespaces** pour inclure toutes les sous-pages
  * cliquer sur le bouton **Export all Pages in Namespace →**

Le téléchargement d'un dossier compressé `DokuWiki-export--20191025-100200.zip` contenant votre wiki commencera.

## Import du wiki

Sur une instance **DokuWiki**, pour **importer** un wiki, vous devez&nbsp;:

  * cliquer sur **Administrer**
  * cliquer sur **DokuWiki Advanced: Import Utility** dans la section **Extensions**
  * cliquer sur le bouton **Parcourir…** pour récupérer l'archive zip (au format `DokuWiki-export--20191025-100200.zip`) sur votre ordinateur
  * cliquer sur **Upload backup file →**&nbsp;:
    1. laissez la valeur par défaut
    * sélectionnez les pages que vous souhaitez importer ou cochez la case à gauche de **File** pour les importer toutes
    * **Optionnellement** vous pouvez cocher **Overwrite existing pages** si vous souhaitez écraser les pages déjà existantes sur votre wiki : si vous venez de créer le wiki pour importer un nouveau, ce n'est pas nécessaire (puisqu'il n'y a rien à écraser) ; si vous avez déjà un wiki, attention&nbsp;: l'import supprimera les pages existantes
  * cliquer sur le bouton **Import**

Votre wiki est alors importé.
