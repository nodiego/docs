# F-Droid
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

F-Droid est un magasin d'applications libres pour les systèmes Android, en alternative à Google Play.

## Installation de F-Droid

Pour permettre l'installation de F-Droid, vous devez changer un paramètre de votre téléphone. Ouvrez l'application **Paramètres**, puis sélectionnez **Sécurité**. Activez l'option **Sources inconnues** le temps de l'installation.

Téléchargez ensuite le fichier d'installation apk de F-Droid et ouvrez-le. Procédez à l'installation en cliquant sur **Installer**.

Une fois l'installation effectuée, retournez dans l'application **Paramètres**, puis **Sécurité** et désactivez l'installation d'applications à partir de sources inconnues.

## Configuration de F-Droid

F-Droid possède une multitude d'options, notamment relatives à la mise à jour de sa base de données, vous pouvez y jeter un œil.

## Utilisation de F-Droid

![F-Droid liste d'applications](../images/FDroid-2.png)

Les applications dans F-Droid sont classées par catégories, et vous pouvez également faire une recherche dans le titre ou la description des applications.
Lorsqu'une application est sélectionnée, vous avez un bouton pour installer l'application.

![F-Droid application](../images/FDroid.png)
