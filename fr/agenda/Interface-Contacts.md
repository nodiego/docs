# Utilisation de l'application contacts
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

Vous pouvez accéder à l'application contacts via le menu disponible en haut à gauche de l'application.
L'application contient la liste de vos contacts sur la partie gauche et le détail de chaque contact sur la partie droite.

![Vue de l'application Contacts](images/contacts-1.png)

## Gestion des contacts
### Création d'un contact

Pour créer un nouveau contact, cliquez sur **Nouveau contact**. La zone de droite est à présent éditable pour le nouveau contact.

Les informations de base (nom, prénom, adresse, téléphone,...) sont remplissables directement, et vous pouvez choisir un champ à ajouter en sélectionnant le champ voulu depuis la liste déroulante en dessous des champs déjà présents.

![Vue de l'édition d'un contact](images/contacts-2.png)

Les informations entrées sont automatiquement sauvegardées dès qu'elles sont entrées, il n'y a donc pas de bouton pour valider.

### Visualisation et édition des informations d'un contact

Cliquez sur un contact pour y accéder et éditer ses informations. De la même manière que lors de la création, les informations sont automatiquement sauvegardées dès lorsqu'elles sont modifiées.

## Gestion des groupes

Pour ajouter un contact à un groupe, éditez la propriété **groupe** d'un contact que vous voulez ajouter au groupe et tapez le nom du groupe, puis cliquez sur la suggestion. Si le nom du groupe n'existe pas, il sera créé. Les groupes s'affichent dans la partie gauche de l'interface.

## Gestion des carnets d'adresses

Vous pouvez créer un nouveau carnet d'adresses en allant dans les paramètres en bas à gauche de l'interface, puis en remplissant le champ **Nom du carnet d'adresses** avant de valider. La liste des carnets d'adresses se trouve en bas à gauche et donne accès à plusieurs actions.

![Vue de la zone de paramètres](images/contacts-3.png)

Les contacts peuvent être mis dans un carnet d'adresses différent en le choisissant dans la liste déroulante en bas des champs. Un contact ne peut pas être mis dans deux carnets d'adresses à la fois (pour le moment).

### Téléchargement du carnet d'adresses

Pour télécharger vos groupes de [contacts](https://framagenda.org/apps/contacts/) vous devez&nbsp;:

  1. cliquer sur **Paramètres** (en bas dans le menu latéral gauche)
  * cliquer sur **…**
  * cliquer sur **Télécharger**

Le carnet d'adresses sera téléchargé au format vCard ('.vcf').

### Lien du carnet d'adresses

Ce lien peut être utile pour la synchronisation des contacts de ce carnet d'adresses sur certaines applications supportant *CardDAV*.

### Partage des carnets d'adresses

Un carnet d'adresses peut-être partagé avec d'autres personnes inscrites sur Framagenda. De la même manière que pour les calendriers, la personne avec qui le carnet d'adresses est partagé verra ce contact dans la liste de ses contacts. S'il n'est pas en mode lecture seule, la personne recevant le partage pourra l'éditer et vous bénéficierez des changements.

### Suppression des carnets d'adresses

Il n'est pas possible de supprimer un carnet d'adresses s'il est seul.

## Import

Pour importer des contacts, cliquez sur les paramètres en bas à gauche de l'interface, puis sur **Importer**. Les seuls fichiers de contacts supportés sont les fichiers vCard *.vcf*. Une fois le fichier choisi, l'importation commence à s'effectuer.
