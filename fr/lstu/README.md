# Framalink / Huitre

[Frama.link](https://frama.link) ou [Huit.re](https://huit.re) est un service en ligne libre et minimaliste qui permet de raccourcir des liens.

  1. Collez le lien à raccourcir dans le formulaire.
  2. Si besoin, choisissez le texte du lien raccourci.
  3. Partagez ensuite avec vos correspondants le lien qui vous est donné.

Le service repose sur le logiciel libre [Lstu](https://lstu.fr/).

---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/990l.jpg" height="340" width="570">
    <source src="https://framatube.org/blip/framalink.mp4" type="video/mp4">
    <source src="https://framatube.org/blip/framalink.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framalink.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/)

## Pour aller plus loin&nbsp;:

  * [Déframasoftiser Internet](deframasoftiser.html)
  * [Essayer Frama.link](https://frama.link) ou [Huit.re](https://huit.re)
  * [Les fonctionnalités](fonctionnalites.md)
