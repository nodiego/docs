# Déframasoftiser Framadrop

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

Le logiciel [lufi](https://lufi.io/) utilisé pour Framadrop peut être installé par d'autres (voir [la documentation](https://framacloud.org/fr/cultiver-son-jardin/lufi.html)), comme par exemple, chez un [CHATON](https://chatons.org/fr/find?title=&field_chaton_services_tid=64&field_chaton_type_tid=All).

<div class="alert-info alert">Les fichiers envoyés sont listés dans le navigateur utilisé pour l'envoi. Vous ne pouvez retrouver les fichiers envoyés depuis un autre navigateur. Les informations sont stockées en localStorage : si vous supprimez vos données localStorage, vous perdrez ces informations.</div>

## Export des données

Pour exporter vos données d'un navigateur vous devez&nbsp;:

  1. vous rendre dans l'onglet **[Mes fichiers](https://framadrop.org/files)**
  * cliquer sur **Exporter les données localStorage**
  * enregistrer le fichier **data.json** sur votre ordinateur

## Importer des données

Après avoir téléchargé le fichier **data.json** sur votre ordinateur, vous pouvez l'importer dans un autre site utilisant le logiciel lufi.

Pour ce faire, vous devez&nbsp;:

  1. vous rendre dans l'onglet **Mes fichiers**
  * cliquer sur **Importer des données localStorage**
  * récupérer votre fichier **data.json**
  * cliquer sur **Ouvrir**

Les fichiers seront alors listés sur le nouveau site. Ils restent cependant **hébergés** sur l'ancien site.
