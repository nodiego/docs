# Déframasoftiser Framanews

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter vos flux RSS de Framanews vous devez&nbsp;:

  * cliquer sur **Actions** <i class="fa fa-caret-down" aria-hidden="true"></i> dans le bord supérieur droit
  * cliquer sur **Configuration…**
  * cliquer sur l'onglet **Flux**
  * cliquer sur la section **OPML**
  * **facultatif** : donner un nom au fichier dans le champ **Nom du fichier**
  * cliquer sur le bouton **Exporter en OPML**
  * enregistrer le fichier `OMPL` sur votre ordinateur

## Importer

Pour importer vos flux dans une nouvelle instance Tiny Tiny RSS (ou tout autre logiciel permettant l'import de fichier OPML), vous devez&nbsp;:

  * cliquer sur **Actions** <i class="fa fa-caret-down" aria-hidden="true"></i> dans le bord supérieur droit
  * cliquer sur **Configuration…**
  * cliquer sur l'onglet **Flux**
  * cliquer sur la section **OPML**
  * cliquer sur **Choose file…**
  * récupérer le fichier `OPML` précédemment téléchargé (voir ci-dessus)
  * cliquer sur le bouton **Importer mon OPML**
  * cliquer sur **Fermer cette fenêtre** de la popup qui apparaît

Vos flux sont importés.

## Supprimer son compte Framanews

Seuls les administrateurs peuvent supprimer un compte. Il faut écrire [ici](https://contact.framasoft.org/#framanews) **en précisant l’instance sur laquelle vous êtes** (https://framanews.org/ttrss/ ou https://framanews.org/ttrss2/) et votre **identifiant**.

La suppression du compte entraîne automatiquement une demande de suppression de votre abonnement à la liste de diffusion dédiée aux utilisateurs de Framanews.
