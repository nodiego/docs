# Prise en main

## Et c’est quoi un flux RSS&nbsp;?

C’est un petit fichier dans un format spécial* qui contient la liste des nouveautés ou des informations du site.

Notez bien que tous les sites ne fournissent pas forcément un flux RSS et s’il le font, il est possible que les articles ne soient pas complets

Vous trouverez la plupart du temps sur les sites une petite icône semblable à celle à droite de ce texte. Il s’agit normalement d’un lien vers le flux RSS du site.

\* Ce format peut être le [RSS](http://fr.wikipedia.org/wiki/RSS) ou le format [Atom](http://fr.wikipedia.org/wiki/Atom). Par abus de langage, on parle juste des flux RSS, c’est plus simple.

## Et comment je fais&nbsp;?

C’est très simple :

  1. Se [créer un compte](https://framanews.org/#modal), relever ses mails, éventuellement regarder dans le dossier des spams puis confirmer son inscription
  - Ajouter des flux RSS
  - Lire ses flux RSS

## C’est si simple&nbsp;?

Basiquement, oui ! Par contre, on peut faire plus de choses très intéressantes (voir la [FAQ Tiny Tiny RSS](FAQ.md) pour plus de détails) :

  * Importer ses flux aux format OPML
  * Créer des catégories de flux
  * Partager les articles
  * …

## La barre d’outils Framarticle

C’est un plugin ttrss ([framarticle_toolbar](https://github.com/framasoft/framarticle_toolbar)) donnant un accès rapide à certaines fonctionnalités de ttrss, en plaçant des boutons d’accès rapides en haut à droite de l’interface.

Sur Framanews, ce plugin est activé par défaut et n’est pas désactivable.

<div class="row">
  <div class="col-md-3">
    <button class="btn btn-default"><i class="fa fa-list" aria-hidden="true"></i></button>
      <p class="lead">Développer ou réduire les articles</p>
      <p>Vous pouvez basculer entre les articles au complet ou juste la liste des titres.</p>
  </div>
  <div class="col-md-3">
      <button class="btn btn-default"><i class="fa fa-eye" aria-hidden="true"></i></button>
      <p class="lead">Marquer les articles comme lus</p><p>
      </p><p>Retour de vacances, pas le temps de tout lire&nbsp;? Zou&nbsp;! Tout les articles affichés sont lus&nbsp;!</p>
  </div>
  <div class="col-md-3">
      <button class="btn btn-default"><i class="fa fa-refresh" aria-hidden="true"></i></button>
      <p class="lead">Rafraîchir le flux</p>
      <p>Des nouveaux articles sont disponibles mais pas encore affichés&nbsp;? Il n’y a qu’à demander.</p>
  </div>
  <div class="col-md-3">
      <button class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i></button>
      <p class="lead">S’abonner au flux</p>
      <p>Raccourci pour la boîte de dialogue d’ajout d’abonnement</p>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
      <button class="btn btn-default"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
      <p class="lead">Article précédent</p>
  </div>
  <div class="col-md-3">
      <button class="btn btn-default"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
      <p class="lead">Prochain article</p>
  </div>
  <div class="col-md-6">
      <button class="btn btn-default"><i class="fa fa-heart" aria-hidden="true"></i></button>
      <p class="lead">Faire un don à <span class="frama">Frama</span><span class="soft">soft</span></p>
      <p>Vous aimez <span class="frama">Frama</span><span class="news">news</span> et vous voulez nous soutenir ? Ce bouton fait apparaître une petite popup contenant le lien vers notre page de soutien.</p>
  </div>
</div>

## Mobilité

Parce que nous sommes tous les jours plus mobiles, Framanews possède un mode tablette, qui conviendra tout aussi bien aux smartphones.

Nous avons aussi installé un client HTML + JavaScript optimisé pour les mobiles mais qui convient aussi pour le bureau : https://framanews.org/enyojs/ (voir sur le [forum de ttrss](https://discourse.tt-rss.org/) pour les sources).

Sinon plusieurs applications pour ttrss existent :

  * [Tiny Tiny RSS]https://git.tt-rss.org/git/tt-rss-android (Android)
  * [TTRSS-Reader](https://github.com/nilsbraden/ttrss-reader-fork) (Android)
  * [YATTRSSC](https://github.com/andreafortuna/YATTRSSC) (webapp, fonctionne aussi sur iOs, et sans doute tous les autres OS mobiles permettant l’utilisation des webapp)
  * [FeedMonkey](https://github.com/jeena/feedmonkey) (webapp, faite au départ pour Firefox OS, devrait fonctionner partout)
  * [TT-RSS Reader](https://itunes.apple.com/fr/app/ttrss-reader-2/id690506281?mt=8) (iOs)

À noter que ces applications nécessitent d’autoriser l’accès par [l’API](FAQ.md#activation-de-lapi) dans les préférences (menu déroulant « Actions », « Configuration »)
