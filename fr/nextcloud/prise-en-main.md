# Prise en main

## Partage par lien

Pour partager un fichier ou un dossier vous devez&nbsp;:

  1. cliquer sur l'icône de partage <i class="fa fa-share-alt"></i> (**si vous ne voyez pas l'icône pensez à désactiver votre bloqueur de pub**)
  * cocher **+** sur la ligne **Lien de partage**
  * cliquer sur l'icône ![icone presse papier](images/nextcloud-copie-icone.png) pour copier le lien à fournir


Pour cesser un partage vous devez&nbsp;:

  1. cliquer sur **Partagé** sur la ligne du dossier ou du fichier
  * cliquer sur <i class="fa fa-ellipsis-h"></i> sur la ligne **Lien de partage**
  * cliquer sur **x Ne plus partager**


En cliquant sur <i class="fa fa-ellipsis-h"></i> vous avez la possibilité de paramétrer les conditions de partage.

<p class="alert-warning alert">Si vous ne voyez pas <b>Partager par lien public</b>, pensez à désactiver votre bloqueur de pub pour Framadrive.</p>

## Télécharger toutes mes données

Vous pouvez à tout moment récupérer l'intégralité de vos données (avant de supprimer votre compte par exemple). Pour cela&nbsp;:

  * cochez la case pour sélectionner tous les fichiers et dossiers
  * cliquez sur <i class="fa fa-ellipsis-h" aria-hidden="true"></i> **Actions**
  * cliquez sur **Télécharger** pour télécharger un dossier *zip* de vos données

![gif montrant comment télécharger ses données drive](images/drive_dl_donnes.gif)
