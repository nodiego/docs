# Astuces

Cette section souhaite répondre aux questions générales liées à l'informatique. Pour des astuces relatives aux services que nous proposons, vous trouverez parfois une section **Astuces** dans leur documentation respective.

## Créer un lien dans un mail

Pour éviter des soucis de copié/collé des liens ou les modifications de ceux-ci par les logiciels mails, vous pouvez insérer un lien dans un mot ou une phrase.

### Avec Thunderbird

Pour ce faire, dans un message, vous devez sélectionner le mot ou la phrase en double-cliquant dessus ou en maintenant le clic, puis&nbsp;:

  * cliquer sur **Insérer**

  ![image montrant comment insérer un lien dans thunderbird](images/astuces_thunderbird_inserer_lien.gif)
  * entrer le lien qui sera lié au mot (ici **site**)

  ![image montrant les propriétés du lien à insérer dans Thunderbird](images/astuces_thunderbird_inserer_lien_proprietes.png)
  * cliquer sur **OK**

La personne recevant le mail aura le lien inséré directement dans le mot (visible dans la barre inférieure en survolant le mot)&nbsp;:

![image montrant un lien survolé](images/astuces_thunderbird_lien_insere.gif)

### Avec gmail

Pour ce faire, vous devez sélectionner le mot ou la phrase en double-cliquant dessus ou en maintenant le clic, puis&nbsp;:

  * cliquer sur l'icône «&nbsp;lien&nbsp;»

  ![image montrant l'icône pour créer un lien](images/astuces_gmail_lien.gif)

  * ajouter le lien à insérer sous **À quelle URL ce lien est-il associé ?**
  * cliquer sur **OK**

  ![image montrant comment insérer le lien dans la phrase sélectionnée](images/astuces_gmail_lien.png)

## Fournir les infos de débogage de DAVx5

Pour aider à comprendre un problème nous pouvons avoir besoin des informations fournies par l'application. Pour les obtenir vous devez&nbsp;:

  1. cliquer sur le menu <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur <i class="fa fa-cog" aria-hidden="true"></i> **Paramètres**
  * cliquer sur <i class="fa fa-bug" aria-hidden="true"></i> **Afficher les infos de débogage**

A partir de là, vous pouvez fournir les informations en cliquant sur l'icône de partage <i class="fa fa-share-alt" aria-hidden="true"></i> et sélectionner votre application mail ou copier les données et les coller dans un [Framabin](https://framabin.org/) et nous donner l'adresse de celui-ci.

## Vider le *localStorage*

### Firefox

Pour vider le *localStorage* d'un site, vous devez utiliser la console intégrée en étant sur celui-ci. Pour ce faire, vous devez&nbsp;:

  1. appuyer sur la touche `F12`
  * cliquer sur l'onglet **Stockage**
  * cliquer sur **Stockage local** dans le menu latéral gauche
  * faire un clic-droit sur le site pour lequel vous souhaitez supprimer le **localStorage**
  * cliquer sur **Tout supprimer**

![vider le localStorage Firefox](images/astuces-localstorage-firefox.png)

### Chrome / Chromium

Pour vider le *localStorage* d'un site, vous devez utiliser la console intégrée en étant sur celui-ci. Pour ce faire, vous devez&nbsp;:

  1. appuyer sur la touche `F12`
  * cliquer sur l'onglet **Application**
  * cliquer sur la flèche à côté de **Local Storage**
  * cliquer droit sur le site lequel vous souhaitez supprimer le **localStorage**
  * cliquer sur **Clear**

![vider le localStorage chrome](images/astuces-localstorage-chrome.png)

## Effacer des cookies

### Firefox

Pour effacer un cookie d'un site avec Firefox, vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-info-circle" aria-hidden="true"></i> à côté de l'adresse du site
  * cliquer sur **Effacer les Cookies et les données de sites…**
  * cliquer sur le site dans la liste
  * cliquer sur le bouton **Supprimer**

![gif pour effacer cookie dans firefox](images/astuces-cookies-firefox.gif)

Voir aussi [la documentation de Firefox à ce sujet](https://support.mozilla.org/fr/kb/effacer-les-cookies-pour-supprimer-les-information#w_effacer-les-cookies-du-site-actuellement-visitae).

### Chrome

Pour effacer un cookie d'un site avec Chrome, vous devez&nbsp;:

  1. cliquer sur l'icône cadenas <i class="fa fa-lock" aria-hidden="true"></i> à côté de l'adresse du site
  * cliquer sur **Cookies**
  * cliquer sur le site dans la liste (ici `framadate.org`)
  * cliquer sur le bouton **Supprimer**
  * cliquer sur le bouton **OK**

![gif pour effacer cookie dans chrome](images/astuces_cookie_chrome.gif)

Voir aussi [la documentation de Chrome à ce sujet](https://support.google.com/chrome/answer/95647).
