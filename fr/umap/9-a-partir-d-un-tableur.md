# Je crée une carte à partir d'un tableur

## Ce que nous allons apprendre

*  Structurer des données pour pouvoir les géocoder
*  Géocoder des adresses et vérifier le résultat
*  Importer un tableau de données dans une carte uMap
*  Injecter le contenu du tableau dans les infobulles
*  Configurer le tri et le filtre des données

## Procédons par étapes

L'objectif de ce tutoriel est de créer une carte en important des données dans uMap. Cela peut être utile dans plusieurs situations :

*  visualiser des données que vous avez trouvées sur un portail open data, par exemple https://data.nantesmetropole.fr/
*  placer sur une carte les contacts (clients, fournisseurs, concurrents...) que vous gérez dans un tableur

L'enjeu est de placer automatiquement chaque élément à sa **position géographique**, définie par une **longitude** et une **latitude** (on parle aussi de **point GPS**). Pour cela il faut que les données soient **géocodées**, par exemple un tableau contiendra deux colonnes : latitude et longitude.

Si les données *open data* sont parfois géocodées, ce n'est généralement pas le cas de votre fichier de contacts qui contient des adresses. Il est dans ce cas nécessaire de passer par une étape de **géocodage**, qui consiste à convertir chaque adresse en latitude et longitude. Nous allons traiter ce cas de figure, étape par étape.

### 1. Je crée un tableau avec des adresses

Pour convertir des adresses en longitude et latitude nous allons utiliser un **géocodeur**. Celui-ci utilise une base de données d'adresses géocodées, parmi lesquelles il recherche l'adresse à géocoder. Voici quelques conseils à suivre pour faciliter la tâche du géocodeur et obtenir de bons résultats :

*  répartir chaque adresse en **plusieurs colonnes** : adresse, code postal et ville
*  porter dans la colonne adresse le **libellé de la voie précédé du numéro**, par exemple ''14 rue de la Paix'', ou le nom du lieu-dit
*  placer tout autre élément d'adresse (boîte postale, étage...) dans un autre champ

Voici par exemple quelques adresses bien structurées, provenant du fichier [Sièges des syndicats intercommunaux de transports scolaires en Loire-Atlantique](https://data.nantesmetropole.fr/explore/dataset/234400034_031-001_sits_shp/information/) :

 | NOM                    | ADRESSE               | COMPL_ADR                   | CP    | VILLE                    |
 | ---                    | -------               | ---------                   | --    | -----                    |
 | C. C. LOIRE et SILLON  | 2 bd de la Loire      |                             | 44260 | SAVENAY                  |
 | C. C. COEUR d'ESTUAIRE | 1 Cours d'Armor       | Route de Savenay            | 44360 | SAINT ETIENNE DE MONTLUC |
 | RESEAU CAP ATLANTIC'   | 4 rue Alphonse Daudet | Zone Tertiaire de Kerbiniou | 44350 | GUERANDE                 |
 | SITS SUD LOIRE LAC     | ZI de la Seiglerie    |                             | 44270 | MACHECOUL                |

L'utilisation de majuscules ou minuscules n'a en général pas d'incidence. Le tableau peut bien sûr contenir d'autres colonnes, comme ici les colonnes NOM et COMPL_ADR.


### 2. Je convertis les adresses en coordonnées géographiques

Plusieurs **géocodeurs** sont disponibles sur internet. La qualité du géocodage peut différer en fonction de plusieurs facteurs :

*  votre adresse est incomplète ou contient une erreur, par exemple un mauvais code postal
*  la base d'adresses utilisée contient des adresses erronées ou n'est pas à jour
*  l'algorithme chargé de comparer votre adresse à celles de la base de données fait de mauvaises hypothèses

Aucun géocodeur n'est parfait. Il est donc important de **vérifier la qualité du géocodage**, voire de comparer et combiner le résultat de plusieurs géocodeurs. La plupart des géocodeurs produisent, en complément à chaque latitude et longitude, un score permettant d'évaluer la qualité du résultat.

En France le site http://adresse.data.gouv.fr donne accès à la Base Adresse Nationale (BAN). Il fournit plusieurs outils, dont le [géocodeur CSV](http://adresse.data.gouv.fr/csv/) qui permet de géocoder une liste d'adresses très rapidement avec de bons résultats.

[DoGeocodeur](https://dogeo.fr/_apps/DoGeocodeur/) est un site particulièrement bien pensé : il sait utiliser plusieurs géocodeurs (Google, IGN, BAN...) et combiner leur résultat, afficher le résultat sur une carte, et vous permet de positionner manuellement une adresse. Lui aussi utilise des fichiers CSV.

<p class="alert alert-info">
CSV désigne un fichier texte contenant les données d'un tableau, dont les valeurs (le contenu de chaque cellule) sont séparées par des virgules (CSV signifie <i>comma-separated values</i>) ... ou par un autre caractère : l'important est que ce <b>séparateur</b> ne soit pas utilisé à l'intérieur d'une valeur. Le point-virgule est souvent utilisé comme séparateur pour créer un fichier CSV.
</p>

Pour géocoder les adresses d'un tableau, les étapes à suivre sont :

 1.  exporter le tableau dans un fichier au format CSV, en choisissant le séparateur (conseil : point-virgule) et le jeu de caractères (encodage) **UTF-8**. Inclure les entêtes de colonnes si l'option vous est proposée. Voici par exemple le panneau d'export CSV de LibreOffice Calc : ![](images/export_csv_libreoffice_calc.png)
 2.  importer le fichier CSV dans le site de géocodage de votre choix, celui-ci vous demande en général de sélectionner les noms de colonnes correspondant à l'adresse, au code postal et à la commune
 3.  vérifier le résultat du géocodage, l'ajuster et le compléter au besoin
 4.  exporter le résultat, qui sera lui aussi au format CSV

### 3. J'importe le tableau géocodé dans un calque

![](images/importer_des_donnees.png)

![](images/umap_edit_import.png) Cliquez sur **Importer des données** pour afficher le panneau du même nom, puis sélectionnez le fichier précédemment géocodé.

Vérifiez que uMap a bien reconnu **CSV** pour le format des données, et choisissez de les importer dans un **nouveau calque**.

Enfin cliquez sur **Importer** : les données sont chargées puis affichées sur la carte. Les lignes n'ayant pas de position géographique sont ignorées, un message est alors affiché.

<p class="alert alert-info">
uMap utilise la première ligne du fichier CSV pour identifier les noms de colonnes, en particulier <b>latitude</b> et <b>longitude</b> qui sont utilisés pour positionner les points (<b>lat</b> et <b>lon</b> sont aussi compris). Vérifiez la présence de ces noms de colonnes si l'opération échoue.
</p>

Notez que vous pouvez directement coller les données dans le panneau d'import. Il est cependant intéressant de passer par un fichier que vous pouvez conserver sur votre poste.

Enfin vous pouvez réimporter les données, par exemple après les avoir mises à jour. Sélectionnez alors le même calque et cochez la case **Remplacer le contenu du calque**.

### 4. J'insère le contenu du tableau dans les infobulles

![](images/infobulle_nom_du_calque.png)

Cliquez maintenant sur un marqueur importé à l'étape précédente : l'infobulle affiche le nom du calque (en l’occurrence le nom du fichier importé si vous ne l'avez pas renommé) au lieu du nom présent dans le tableau de données.

Il y a plusieurs possibilités pour remédier à cela.

#### Modifier le champ utilisé

![](images/cle_du_libelle.png)

Éditez le calque et modifiez, dans l'onglet Propriétés avancées, la **Clé pour le libellé**. Saisissez le nom de la colonne du fichier importé. Chaque infobulle affiche désormais le contenu de cette colonne.

![](images/infobulle_nom_correct.png)

<p class="alert alert-info">
Respecter la casse, c'est-à-dire les majuscules et minuscules. Le nom de colonne ne doit pas contenir de caractère spécial : accents, espace, ponctuation...
</p>

#### Afficher un tableau

![](images/popup_tableau.png)

Le contenu du tableau peut être affiché dans les infobulles, sous forme d'un tableau à deux colonnes : l'intitulé et la valeur correspondante.

Dans l'onglet **Options d'interaction** du calque, changez le **Style de la popup** en **Tableau**. Voici un exemple de résultat :

![](images/infobulle_tableau.png)

![](images/modifier_tableau.png)

Notez que vous pouvez modifier le contenu du tableau en cliquant sur **Éditer dans un tableau** dans le sélecteur de calques. Vous pouvez alors supprimer ou renommer des colonnes, voire même modifier les cellules du tableau.

#### Définir le gabarit des infobulles

![](images/gabarit_popup.png)

Le tableau ci-dessus n'est pas particulièrement joli avec ses libellés en majuscules.

Dans le tutoriel [Je crée des infobulles multimédia](5-infobulles-multimedia.html) nous avons vu comment formater le contenu d'une infobulle. Nous pouvons utiliser la même syntaxe pour définir le contenu de **toutes les infobulles d'un calque**, en y intégrant le contenu des cellules du tableau.

Dans l'onglet **Options d'interaction** du calque, éditez le **Gabarit du contenu de la popup**.  Définissez le format des popups (titres, caractère gras, etc.) comme vu précédemment. Pour *injecter* le contenu d'une cellule dans l'infobulle, il suffit d'ajouter le nom de la colonne placé entre accolades, par exemple **{NOM}**.

![](images/infobulle_avec_gabarit.png)

Vous pouvez utiliser dans le gabarit tous les champs du tableau. Voici à droite un exemple de gabarit et le résultat pour une infobulle.

**Cette approche est très puissante.** Vous pouvez l'utiliser pour injecter, pour chaque ligne du tableau, un lien vers un site internet (et pourquoi pas le texte associé), une image ou une iframe. Il suffit pour cela d'intégrer le nom de colonnes et ses accolades, à la syntaxe de mise en forme du texte, par exemple

	[{TEXTE_LIEN}]({LIEN_SITE})

ou encore

	{{{URL_IMAGE}}}

### 5. Je configure le tri et les filtres

![](images/config_filtres.png)

Nous avons vu dans le tutoriel [Je consulte une carte uMap](1-consulter.html) qu'il est possible de voir l'ensemble des données de la carte sous la forme d'une liste. Cette liste peut aussi filtrée par l'utilisateur, à partir d'un mot par exemple.

![](images/umap_edit_props.png) Pour permettre aux utilisateurs de filtrer les données il convient de préciser à uMap à quel(s) champ(s) le "mot-filtre" doit s'appliquer. Cela s'effectue dans l'onglet **Propriétés par défaut** des **Propriétés de la carte**. Vous pouvez indiquer plusieurs noms de champ (noms de colonne), le filtre s'appliquera à chacun des champs.

Notez que vous pouvez également trier la liste en sélectionnant la **Clé de tri**, c'est-à-dire le nom de la colonne utilisée pour le tri (tri croissant uniquement). Enfin vous pouvez définir la clé par défaut pour le libellé, qui sera utilisée si celle-ci n'est pas définie pour le calque.

<p class="alert alert-info">
Les clés de tri et de filtre s'appliquent à l'ensemble des données, tous calques confondus. Si votre carte est constituée de plusieurs calques, il est donc conseillé d'utiliser le même nom de clé pour désigner le même type d'information. Évitez par exemple d'utiliser <b>Ville</b> pour un calque et <b>Commune</b> pour un autre, utilisez plutôt <b>Commune</b> pour les deux.
</p>

## Faisons le point

Ce tutoriel est probablement le plus complexe de la série. Appréciez toutefois les possibilités offertes par uMap pour intégrer des données extérieures.
